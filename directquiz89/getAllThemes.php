<?php

include ("config.php");

try {
    // Connexion à la base de données avec PDO
    $pdo = new PDO("mysql:host=$ADRES;dbname=$BASE;charset=utf8", $USER, $MDP);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Appel de la procédure stockée
    $stmt = $pdo->prepare("CALL DQ_AllThemesOfficiels()");
    $stmt->execute();

    // Récupération des résultats
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    // Conversion en JSON
    header('Content-Type: application/json');
    echo json_encode($result);

} catch (PDOException $e) {
    // Gestion des erreurs
    http_response_code(500);
    echo json_encode(['error' => $e->getMessage()]);
}
?>