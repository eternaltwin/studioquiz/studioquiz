<?php

function getFrequentationsBienEcrit($json)
{
	
	// URL cible de la requête POST
		$url = 'https://p8091.***************/openai'; 

		// Paramètres à envoyer dans la requête POST
		$data = array(
			'systeme' => 'Tu vas recevoir en entrée un JSON reprenant les 3 meilleures tranches horaires d\'un jeu en ligne de quiz, fusionne le tout en une seule tranche horaire ou plusieurs si elles sont contigues et en commençant par : "Si vous n\'avez personne, le meilleur moment pour venir actuellement est..."',
			'contenu' => $json
		);

		// Initialisation de cURL
		$ch = curl_init($url);

		// Configuration des options de cURL
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

		// Exécution de la requête
		$response = curl_exec($ch);

		// Vérification des erreurs
		if($response === false) {
			echo 'Erreur cURL';
		}

		// Fermeture de la ressource cURL
		curl_close($ch);

		// Affichage de la réponse
		return $response;

}


// Connexion à la base de données
include('config.php');

// Création de la connexion
$conn = new mysqli($ADRES, $USER, $MDP, $BASE);

// Vérification de la connexion
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Appel de la procédure stockée
$sql = "CALL DQ_GetMeilleuresFrequentations()";
$result = $conn->query($sql);

if ($result === false) {
    die("Erreur lors de l'appel de la procédure stockée : " . $conn->error);
}

// Création d'un tableau pour stocker les résultats
$rows = array();

// Parcours des résultats et stockage dans le tableau
while ($row = $result->fetch_assoc()) {
    $rows[] = $row;
}

// Libération de la mémoire du résultat
$result->free();

// Fermeture de la connexion
$conn->close();

// Conversion du tableau en format JSON
$json_output = json_encode($rows);

// Envoi du résultat JSON

echo getFrequentationsBienEcrit($json_output);

/*
	header('Content-Type: application/json');
	echo $json_output;
*/

?>
