<?php
header('Content-type: text/html; charset=UTF-8');
include('config.php');

	$mysqli = new mysqli($ADRES, $USER, $MDP, $BASE);
	$mysqli->set_charset("utf8mb4");
	
	if ($mysqli->connect_errno) {
		echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}

		$theme = $_POST['theme'];
		$nombre = $_POST['nombre'];
		
		$query = "CALL DQ_UpdateOnlinePlayer($theme, $nombre);";
		
		$mysqli->query($query);

		/* Fermeture de la connexion */
		$mysqli->close();
	
		$mysqli = null;
		
		echo $theme.' | '.$nombre;
	
?>