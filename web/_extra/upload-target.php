<?php
$myfile = fopen("log.txt", "a+") or die("Unable to open file!");

  // Grab any extra form data!

  foreach ($_POST as $key => $val) {
    echo $key . ": " . $val . "\n";
  }

  echo "\n\n";

  // Remember to process the uploads!

  $error = false;
  try
  {
    if(isset($_FILES['file'])){
      $errors= array();
      $file_name = $_FILES['file']['name'];
	  $file_name = str_replace(' ', '', $file_name);
	  
      $file_size =$_FILES['file']['size'];
      $file_tmp =$_FILES['file']['tmp_name'];
      $file_type=$_FILES['file']['type'];
      $file_ext=strtolower(end(explode('.',$_FILES['file']['name'])));
      
      $extensions= array("jpeg","jpg");
      
      if(in_array($file_ext,$extensions)=== false){
         $errors[]="extension not allowed, please choose a JPEG file.";
		  	fwrite($myfile, "extension not allowed, please choose a JPEG file.\n");
      }
      
      if($file_size > 5097152){
         $errors[]='File size must be exactely 2 MB';
		 	fwrite($myfile, "2MB Error\n");
      }
      
      if(empty($errors)==true){
         move_uploaded_file($file_tmp,"images/avatar/".$file_name);
         echo "Success";
      }else{
		 	fwrite($myfile, $errors."\n");
      }
   }
  }

  catch (Exception $e) {
	fwrite($myfile, 'Exception reçue : ',  $e->getMessage(), "\n");
	
}
  if ($error) {
    die("Error: " . $error);
	fwrite($myfile, $error);
  } else {
    die("File: " . $file);
  }
  
  fclose($myfile);

?>