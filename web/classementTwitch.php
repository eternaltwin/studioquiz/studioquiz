<!DOCTYPE html>
<html lang="fr">
<!-- Basic -->

<head>
	<base href="/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title>CLASSEMENT STREAM QUIZ</title>

	<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">

	<!-- Datatable CSS -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.11/css/dataTables.dataTables.min.css" />

        <!-- Site CSS -->
        <link rel="stylesheet" href="css/style.css">

	<style>

        body {
            font-family: 'Poppins', sans-serif;
            color: #5d4747;
            font-size: 16px;
            line-height: 30px;
            font-weight: 400;
            margin: 0;
            padding: 0;
            background-color: #0c1029;
            background-image: url('images/bg.jpg');
            background-repeat: no-repeat;
            background-size: 100%;
        }

        .nom-pres {
            font-family: 'Anton', sans-serif;
        }

        .twitch-game {
            padding-top: 20px;
        }

        .white_fonts {
            color: white;
        }

        .pres-cadre{
            padding-top: 30px;
            z-index: 4;
        }

        .logo-main-cadre {
            padding-top: 90px;
            padding-left: 30px;
            z-index: 10;
        }

		th
		{
			
			font-size: 0.8em!important;
            background-color:#f5b300 !important;
            border: 1px solid #8f2900 !important;
		}

        td {
            background-color:#0f1635 !important;
            border: 1px solid #8f2900 !important;
        }

        th, td{
            height: 24px !important;
        }

        .avatar-classement-level, .avatar-classement
        {

            height: 20px;
            width: 20px;
            float: left;

        }

        .avatar-classement-level
        {
            position: relative !important;
            left: -30px !important;
        }

        table
        {
           
            width: 100% !important;
            color: white !important;

        }

        .pseudo
        {
            position: relative;
            left: -15px;
        }
        
       
        .container-table
        {
            background-color:#f5b300;
            border: 5px solid white;
            border-radius:40px;
            margin-left: 50px;
            margin-right: 50px;
            padding-top: -2px;
            overflow: hidden;
            position: relative !important;
            z-index: 10;
        }

        th{
            border-top: 0 !important;     
        }

        th:first-child {
            border-left: 0 !important;     
        }

        th:last-child {
            border-right: 0 !important;     
        }

        th{
            border-top: 0 !important; 
            text-align: center !important;    
        }
       

        .img-twitch {
            height: 200px;
        }

        .twitch-left
        {
            margin-left: 100px;
        }

        .twitch-top-left
        {
            margin-top: 80px;
            margin-left: 50px;
        }

        #classement_wrapper
        {           
            width: 100%;

        }

        #eclairage
        {
            background-image : url('images/lueur.png');
            width: 100%;
            height: 100%;
            position: absolute;
            z-index: 5;
        }

        .logo-twitch-cadre, .logo-main-cadre
        {
            z-index: 10;
        }

        input[type='search']::placeholder
        {
            color: white;
        }

        @font-face {
            font-family: 'Anton';
            src: url('/fonts/Anton.ttf') format('ttf');
            font-weight: normal;
            font-style: normal;
        }

        input[type='search']
        {
            color: white;
            background-color: #f5b300 !important;
            font-family: 'Anton', sans-serif;
            font-size: 1.2em;
            text-align: center;
            display: block;
            border: 1px solid #8f2900;
        }

        .dataTables_info
        {
            display: none;
        }

        .paginate_button .current
        {
            display: none;
        }

        table.dataTable tbody td
        {
            padding: 3px !important;
            text-align: center;
        }

        table.dataTable tbody th
        {
            padding: 3px !important;
            text-align: center;
        }

        #prev-custom > a, #next-custom > a
        {
            display: block;
            color: white;
            background-color: #f5b300 !important;
            font-size: 1em;
            text-align: center;
            border: 1px solid #8f2900;
            width: 100px;
            cursor: pointer;
        }

        #prev-custom > a
        {
            margin-left: 50px;
        }

        #next-custom > a
        {
            margin-right: 50px;
            float: right;
        }

        .nav-bar-data
        {
            padding-top: 5px;
            padding-bottom: 5px;
        }

        #logo-pres
        {
           margin-top: -30px;
        }

	</style>
		
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>	
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.3/js/bootstrap.bundle.min.js" integrity="sha512-7Pi/otdlbbCR+LnW+F7PwFcSDJOuUJB3OxtEHbg4vSMvzvJjde4Po1v4BR9Gdc9aXNUNFVUY+SK51wWT8WF0Gg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.datatables.net/1.13.11/js/jquery.dataTables.min.js"></script>

	<script>
	
    // Récupération de l'URL actuelle
    const url = new URL(window.location.href);

    var jdt = null;

    function setTheme(theme) {   

        setJqueryDataTable(theme);
        
        $("#logo-twitch").css("width", ($(window).width() / 2 / 2.5) + "px");
        $("#logo-pres").css("width", ($(window).width() / 2 / 3) + "px");
        $(".nom-pres").css("font-size", (($(window).width() / 2 / 2.5)/8.5) + "px");
        $("#logo-main").css("width", (($(window).width() / 2 / 2.5)*1.6) + "px");
        $("#logo-theme").css("width", (($(window).width() / 2 / 2.5)*1.45) + "px");

        $(window).resize(function() {

            $("#logo-twitch").css("width", ($(window).width() / 2 / 2.5) + "px");
            $("#logo-pres").css("width", ($(window).width() / 2 / 3) + "px");
            $(".nom-pres").css("font-size", (($(window).width() / 2 / 2.5)/8.5) + "px");
            $("#logo-main").css("width", (($(window).width() / 2 / 2.5)*1.6) + "px");
            $("#logo-theme").css("width", (($(window).width() / 2 / 2.5)*1.45) + "px");

        });  

        $("#logo-theme").attr("src", "images/theme-" + theme + ".png");
        $("#logo-pres").attr("src", "images/pres-" + theme + ".png");

        switch (theme) {
            case '0':
            $("#label-nom-pres").html("&nbsp;"); $("#label-nom-pres").css("font-size", (($(window).width() / 2 / 2.5)/8.5) + "px");
            break;
            case '1':
            $("#label-nom-pres").text("MARC MAZZOTI"); $("#label-nom-pres").css("font-size", (($(window).width() / 2 / 2.5)/8.5) + "px");
            break;
            case '2':
            $("#label-nom-pres").text("CHARLOTTE AUFREST"); $("#label-nom-pres").css("font-size", (($(window).width() / 2 / 2.5)/10) + "px");
            break;
            case '3':
            $("#label-nom-pres").text("JAMES E. NORRAY"); $("#label-nom-pres").css("font-size", (($(window).width() / 2 / 2.5)/10) + "px");
            break;
            default:
            $("#label-nom-pres").text("MARC MAZZOTI"); $("#label-nom-pres").css("font-size", (($(window).width() / 2 / 2.5)/8.5) + "px");
            break;
        }

    }    


	$(function(){
	
        let currentTheme = (url.searchParams.get('theme') == null? 0 : url.searchParams.get('theme'));

        setTheme(currentTheme);  

        window.resizeTo(1280, 720);
		
	});

    function go_next()
    {
 
        var pageInfo = jdt.page.info();

        // Vérifier si ce n'est pas la dernière page
        if (pageInfo.page < pageInfo.pages - 1) {
            jdt.page('next').draw('page');
        }

    }

    function go_prev()
    {

        var pageInfo = jdt.page.info();

        // Vérifier si ce n'est pas la première page
        if (pageInfo.page > 0) {
            jdt.page('previous').draw('page');
        }
        
    }
	
	  function setJqueryDataTable(theme) {

            jdt = $("#classement").DataTable({
				order: [2, "desc"],
				pageLength: 7,
                serverSide: true,
                processing: true,
				paging  : true,
				dom: 'Brftip',
				language: {
					url: "js/DT_French.json"
				},
                ajax: {
                    url: 'listClassementTwitch.php?theme='+theme, // Lien vers le script PHP qui renvoie les données
                    type: 'POST',
					datatype: 'json',					
					dataSrc: function(response) {
						return response.data;
					},
					error: function(xhr, errorType, exception) {
						console.error("Erreur AJAX : " + xhr.status + " - " + exception);
					}
                },
				columnDefs: [
				{
					"data": 'position', 
					"title":'#',
                    "targets": [0],
                    "orderable": false,
                    "searchable": false,
					"width" : 30
                },
                {
					"data": 'pseudo', 
					"title":'JOUEUR',
                    "targets": [1],
                    "orderable": true,
                    "searchable": true,
					"width" : 150,
					"render": function (data, type, row) {

						return '<img alt="avatar" class="avatar-classement" src="'+ row.avatar +'" /><a target="_BLANK" href="https://www.twitch.tv/'+row.pseudo+'"><img alt="grade" class="avatar-classement-level" src="images/avatar/level_' + row.level + '.png" /></a> <span class="pseudo">' + row.pseudo + '</span>';

					}
                },
			    {
					"data": 'points_totaux', 
					"title":'POINTS TOTAUX',
                    "targets": [2],
                    "orderable": true,
                    "searchable": false,
                    "width" : 110
                },
                {
					"data": 'best_score', 
					"title":'MEILLEUR SCORE',
                    "targets": [3],
                    "orderable": true,
                    "searchable": false,
                    "width" : 110
                },
				{
					"data": 'moyenne', 
					"title":'SCORE MOYEN',
                    "targets": [4],
                    "orderable": true,
                    "searchable": false,
                    "width" : 110
                },
                {
					"data": 'last_game', 
					"title":'DERNIÈRE PARTIE',
                    "targets": [5],
                    "orderable": false,
                    "searchable": false,
                    "width" : 140
                },
                {
					"data": 'game_done', 
					"title":'PARTIES JOUÉES',
                    "targets": [6],
                    "orderable": true,
                    "searchable": false,
                    "width" : 105
                },
                {
					"data": 'favourite_theme', 
					"title":'♥',
                    "targets": [7],
                    "orderable": true,
                    "searchable": false,
                    "width" : 10,                    
					"render": function (data, type, row) {

                        switch (row.favourite_theme)
                        {
                            case '1':
                                return '<img src="images/smiley_marc.png" data-toggle="tooltip" data-placement="top" title="MEGAQUIZ" alt="MEGAQUIZ" />';
                                break;
                            case '2':
                                return '<img src="images/smiley_charlotte.png" data-toggle="tooltip" data-placement="top" title="MUSIQUE - GEEK" alt="MUSIQUE - GEEK" />';
                                break;
                            case '3':
                                return '<img src="images/smiley_james.png" data-toggle="tooltip" data-placement="top" title="TV - CINEMA" alt="TV - CINEMA" />';
                                break;
                            default:
                                return '<img src="images/smiley_rien.png" data-toggle="tooltip" data-placement="top" title="-" alt="-" />';
                        }

					}
                }

                ],
				    lengthMenu: [10, 15] // Options de sélection du nombre de lignes par page				
            }).on('draw',function(){
			


                
          

                    $("#classement_previous").clone().appendTo("#prev-custom");
                    $("#classement_previous").remove();

                    $("#classement_filter").find('input').clone().appendTo("#search-custom");
                    $("#classement_filter").remove();
                    $("input[type='search']").attr("placeholder", "RECHERCHER UN JOUEUR").addClass('mx-auto'); 

                    $("#classement_next").clone().appendTo("#next-custom");
                    $("#classement_next").remove();

                     $("#classement_paginate").remove();

                     $("input[type='search']").on( 'keyup', function () {
                        jdt.search($(this).val()).draw();
                    });

                    $('[data-toggle="tooltip"]').tooltip();

				// Do nothing
			
			});
			
        }
		
		</script>
	
</head>

<body>

  <div id="eclairage"></div>

            <div class="row">
                <div class="col-2 twitch-game logo-twitch-cadre">
                   <a href="https://www.twitch.tv/leplateautv" target="_BLANK"><img id="logo-twitch" class="twitch-left" src="images/logo-twitch.png" /></a>
                   <br/>
                   <img id="logo-theme" class="twitch-top-left" src="images/theme-1.png" />
                </div>
                <div class="col-2 offset-3 twitch-game pres-cadre">   
                        <center>             
                            <span id="label-nom-pres" class="white_fonts nom-pres">MARC MAZZOTI</span>
                            <img id="logo-pres" src="images/pres-1.png" />
                        </center>
                </div>
                <div class="col-3 twitch-game logo-main-cadre">
             
                        <img id="logo-main" src="images/logo-qs.png" />
  
                </div>
            </div>

   <div class="row">

        <div class="col-12">

            <div class="container-table">               
                
                    <table id="classement" class="cell-border">
                        <thead>
                            <tr><th>#</th><th>Joueur</th><th>Points totaux</th><th>Meilleur score</th><th>Score moyen</th><th>Parties jouées</th><th>Dernière partie</th><th>Salon favori</th></tr>
                        </thead>
                    
                        <tbody>   
                            <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
                        </tbody>
                    </table>
                    <div class="row nav-bar-data">
                        <div id="prev-custom" onclick="go_prev();" class="col-4">
                        </div>
                        <div id="search-custom" class="col-4">
                        </div>
                        <div id="next-custom" onclick="go_next();" class="col-4">
                        </div>
                   </div>
                                
            </div>
         </div>

   </div>
  


    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

</body>

</html>