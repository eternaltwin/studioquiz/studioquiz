<?php
// Inclure le fichier de configuration
include 'config.php';

// Configurer les en-têtes pour la réponse JSON
header('Content-Type: application/json');

try {
    // Vérifier si les paramètres 'id' et 'expression' sont fournis
    if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
        throw new Exception("Le paramètre 'id' est manquant ou invalide.");
    }
    if (!isset($_GET['expression']) || strlen($_GET['expression']) > 255) {
        throw new Exception("Le paramètre 'expression' est manquant ou dépasse 255 caractères.");
    }

    // Récupérer les paramètres
    $p_num_theme = (int)$_GET['id'];
    $p_like_exp = $_GET['expression'];

    // Connexion à la base de données
    $dsn = "mysql:host=$ADRES;dbname=$BASE;charset=utf8mb4";
    $pdo = new PDO($dsn, $USER, $MDP);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Appeler la procédure stockée avec les paramètres
    $stmt = $pdo->prepare("CALL DQ_AddThemeClassification(:p_num_theme, :p_like_exp)");
    $stmt->bindParam(':p_num_theme', $p_num_theme, PDO::PARAM_INT);
    $stmt->bindParam(':p_like_exp', $p_like_exp, PDO::PARAM_STR);
    $stmt->execute();

    // Vérifier le succès de l'opération
    $stmt->closeCursor();

    // Retourner une réponse JSON indiquant le succès
    echo json_encode(['success' => true, 'message' => "Classification ajoutée avec succès."]);
} catch (PDOException $e) {
    // Gestion des erreurs de base de données
    echo json_encode(['success' => false, 'error' => $e->getMessage()]);
} catch (Exception $e) {
    // Gestion des erreurs générales
    echo json_encode(['success' => false, 'error' => $e->getMessage()]);
}
?>
