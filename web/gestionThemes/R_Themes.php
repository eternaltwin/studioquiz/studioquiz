<?php
// Inclure le fichier de configuration
include 'config.php';

// Configurer les en-têtes pour la réponse JSON
header('Content-Type: application/json');

try {
    // Connexion à la base de données
    $dsn = "mysql:host=$ADRES;dbname=$BASE;charset=utf8mb4";
    $pdo = new PDO($dsn, $USER, $MDP);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Appeler la procédure stockée
    $stmt = $pdo->prepare("CALL DQ_SelectThemes()");
    $stmt->execute();

    // Récupérer les résultats
    $themes = [];
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $themes[] = [
            'IDTheme' => $row['IDTheme'],
            'LibelleTheme' => $row['LibelleTheme']
        ];
    }

    // Fermer le curseur
    $stmt->closeCursor();

    // Retourner les résultats au format JSON
    echo json_encode(['success' => true, 'data' => $themes]);
} catch (PDOException $e) {
    // Gestion des erreurs
    echo json_encode(['success' => false, 'error' => $e->getMessage()]);
}
?>
