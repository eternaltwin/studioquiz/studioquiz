<?php
// Inclure le fichier de configuration
include 'config.php';

// Configurer les en-têtes pour la réponse JSON
header('Content-Type: application/json');

try {
    // Vérifier si le paramètre 'id' est fourni et est un entier
    if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
        throw new Exception("Le paramètre 'id' est manquant ou invalide.");
    }

    // Récupérer le paramètre
    $p_theme = (int)$_GET['id'];

    // Connexion à la base de données
    $dsn = "mysql:host=$ADRES;dbname=$BASE;charset=utf8mb4";
    $pdo = new PDO($dsn, $USER, $MDP);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Appeler la procédure stockée avec le paramètre
    $stmt = $pdo->prepare("CALL DQ_SelectThemeClassificationByTheme(:p_theme)");
    $stmt->bindParam(':p_theme', $p_theme, PDO::PARAM_INT);
    $stmt->execute();

    // Récupérer les résultats
    $classifications = [];
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $classifications[] = [
            'IDMapping' => $row['IDMapping'],
            'Like_expression' => $row['Like_expression'],
            'Hybride' => $row['Hybride']
        ];
    }

    // Fermer le curseur
    $stmt->closeCursor();

    // Retourner les résultats au format JSON
    echo json_encode(['success' => true, 'data' => $classifications]);
} catch (PDOException $e) {
    // Gestion des erreurs de base de données
    echo json_encode(['success' => false, 'error' => $e->getMessage()]);
} catch (Exception $e) {
    // Gestion des erreurs générales
    echo json_encode(['success' => false, 'error' => $e->getMessage()]);
}
?>
