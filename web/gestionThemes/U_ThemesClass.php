<?php
// Inclure le fichier de configuration
include 'config.php';

// Configurer les en-têtes pour la réponse JSON
header('Content-Type: application/json');

try {
    // Vérifier si les paramètres 'theme', 'exp', et 'IDMapping' sont fournis et valides
    if (!isset($_GET['theme']) || !is_numeric($_GET['theme'])) {
        throw new Exception("Le paramètre 'theme' est manquant ou invalide.");
    }
    if (!isset($_GET['exp']) || strlen($_GET['exp']) > 255) {
        throw new Exception("Le paramètre 'exp' est manquant ou dépasse 255 caractères.");
    }
    if (!isset($_GET['IDMapping']) || !is_numeric($_GET['IDMapping'])) {
        throw new Exception("Le paramètre 'IDMapping' est manquant ou invalide.");
    }
    if (!isset($_GET['Hybride'])) {
        throw new Exception("Le paramètre 'Hybride' est manquant ou invalide.");
    }

    // Récupérer les paramètres
    $p_theme = (int)$_GET['theme'];
    $p_expression = $_GET['exp'];
    $p_id_mapping = (int)$_GET['IDMapping'];
    $p_hybride = $_GET['Hybride'];

    // Connexion à la base de données
    $dsn = "mysql:host=$ADRES;dbname=$BASE;charset=utf8mb4";
    $pdo = new PDO($dsn, $USER, $MDP);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Appeler la procédure stockée avec les paramètres
    $stmt = $pdo->prepare("CALL DQ_UpdateThemeClassification(:p_theme, :p_expression, :p_id_mapping, :p_hybride)");
    $stmt->bindParam(':p_theme', $p_theme, PDO::PARAM_INT);
    $stmt->bindParam(':p_expression', $p_expression, PDO::PARAM_STR);
    $stmt->bindParam(':p_id_mapping', $p_id_mapping, PDO::PARAM_INT);
    $stmt->bindParam(':p_hybride', $p_hybride, PDO::PARAM_STR);
    $stmt->execute();

    // Vérifier le succès de l'opération
    $stmt->closeCursor();

    // Retourner une réponse JSON indiquant le succès
    echo json_encode(['success' => true, 'message' => "Classification mise à jour avec succès."]);
} catch (PDOException $e) {
    // Gestion des erreurs de base de données
    echo json_encode(['success' => false, 'error' => $e->getMessage()]);
} catch (Exception $e) {
    // Gestion des erreurs générales
    echo json_encode(['success' => false, 'error' => $e->getMessage()]);
}
?>
