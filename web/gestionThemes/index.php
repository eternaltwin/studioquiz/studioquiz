<?php
session_start();

// Code secret requis
$code_secret = "521437"; // Remplace par ton code

// Vérifie si l'utilisateur a déjà fourni le code correct
if (!isset($_SESSION['acces_autorise']) || $_SESSION['acces_autorise'] !== true) {
    // Si le formulaire est soumis, on vérifie le code
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['code']) && $_POST['code'] === $code_secret) {
            // Code correct, on accorde l'accès
            $_SESSION['acces_autorise'] = true;
            header("Location: " . $_SERVER['PHP_SELF']); // Redirection pour éviter le resoumission du formulaire
            exit;
        } else {
            $erreur = "Code incorrect. Veuillez réessayer.";
        }
    }

    // Affiche le formulaire si l'accès n'est pas encore autorisé
    ?>
    <!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Accès Restreint</title>
        <style>
            body {
                font-family: 'Arial', sans-serif;
                background: linear-gradient(135deg, #1e90ff, #87cefa);
                color: #fff;
                display: flex;
                justify-content: center;
                align-items: center;
                height: 100vh;
                margin: 0;
            }
            .container {
                text-align: center;
                background: rgba(255, 255, 255, 0.1);
                backdrop-filter: blur(10px);
                padding: 20px 40px;
                border-radius: 15px;
                box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.2);
                max-width: 400px;
                width: 90%;
            }
            h1 {
                font-size: 2rem;
                margin-bottom: 1rem;
            }
            p {
                margin-bottom: 1.5rem;
            }
            .error {
                color: #ffcccc;
                margin-bottom: 1rem;
                font-weight: bold;
            }
            form {
                display: flex;
                flex-direction: column;
            }
            label {
                font-weight: bold;
                margin-bottom: 0.5rem;
            }
            input {
                padding: 10px;
                font-size: 1rem;
                border: none;
                border-radius: 5px;
                margin-bottom: 1rem;
                outline: none;
            }
            input:focus {
                box-shadow: 0 0 5px rgba(30, 144, 255, 0.8);
            }
            button {
                padding: 10px 15px;
                font-size: 1rem;
                color: #fff;
                background: #1e90ff;
                border: none;
                border-radius: 5px;
                cursor: pointer;
                transition: background 0.3s ease;
            }
            button:hover {
                background: #4682b4;
            }
            footer {
                margin-top: 1rem;
                font-size: 0.9rem;
                opacity: 0.8;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Gestion des thèmes - Accès Restreint 🔒 </h1>
            <p>Veuillez entrer le code pour accéder à cette page :</p>
            <?php if (isset($erreur)) { echo "<div class='error'>$erreur</div>"; } ?>
            <form method="post">
                <label for="code">Code d'accès</label>
                <input type="password" id="code" name="code" placeholder="Entrez votre code" required>
                <button type="submit">Valider</button>
            </form>
            <footer>[ PAGE EN ACCÈS LIMITÉ ]</footer>
        </div>
    </body>
    </html>
    <?php
    exit; // Stoppe l'exécution du reste de la page
}
?>



<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gestionnaire de Thèmes</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="app-container">
        <header class="app-header">
            <div class="header-content">
                <h1><i class="fas fa-layer-group"></i> Gestionnaire de Thèmes</h1>
                <p class="header-subtitle">Classification et organisation des thèmes</p>
            </div>
        </header>

        <main class="app-main">
            <div class="content-wrapper">
                <section id="themes" class="section-container">
                    <div class="section-header">
                        <h2><i class="fas fa-th-list"></i> Thèmes Disponibles</h2>
                    </div>
                    <div class="themes-grid" id="theme-list"></div>
                </section>

                <section id="classifications" class="section-container" style="display: none;">
                    <div class="section-header">
                        <button id="back-to-themes" class="btn btn-secondary">
                            <i class="fas fa-arrow-left"></i> Retour
                        </button>
                        <h2>Classifications pour : <span id="current-theme" class="highlight-text"></span></h2>
                    </div>

                    <div class="classifications-container">
                        <div id="classification-list" class="classification-grid"></div>
                        
                        <form id="add-classification-form" class="add-form">
                            <div class="input-group">
                                <input type="text" 
                                       id="new-expression" 
                                       placeholder="Nouvelle expression..." 
                                       maxlength="255" 
                                       required>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fas fa-plus"></i> Ajouter
                                </button>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </main>

        <footer class="app-footer">
            <p>&copy; 2025 Gestionnaire de Thèmes - Tous droits réservés</p>
        </footer>
    </div>

    <div id="notification" class="notification"></div>

    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script src="scripts.js"></script>
</body>
</html>