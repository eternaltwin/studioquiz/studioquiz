  
      // État global
      let selectedThemeId = null;
      let selectedThemeName = null;
  
  // Gestionnaire de notifications
    const notification = {
        show: function(message, type = 'success') {
            const notif = $('#notification');
            notif.text(message)
                .removeClass('success error')
                .addClass(type)
                .addClass('show');
            
            setTimeout(() => {
                notif.removeClass('show');
            }, 3000);
        }
    };

function nettoyerTheme(IDTheme)
{

    if (confirm("Voulez-vous vraiment nettoyer ce thème ?")) {
        $.getJSON("D_ThemeClass.php", {
            idMapping: 0,
            idTheme: IDTheme
        })
        .done(function (response) {
            if (response.success) {
                //loadClassifications(themeId);
                notification.show("Thème nettoyé avec succès.");
            } else {
                notification.show(response.error || "Erreur lors du nettoyage.", 'error');
            }
        })
        .fail(function() {
            notification.show("Erreur de connexion au serveur.", 'error');
        });
    }



}

function updateHybrideStatus(status, idMapping, oldExpression)
{
    $.getJSON("U_ThemesClass.php", {
        theme: selectedThemeId,
        exp: oldExpression,
        IDMapping: idMapping,
        Hybride: status
    })
    .done(function (response) {
        if (response.success) {
            loadClassifications(selectedThemeId, selectedThemeName);
            notification.show(`Cette classification ${(status == 'Y' ?"est désormais hybride":"n'est plus hybride")}.`);
        } else {
            notification.show(response.error || "Erreur lors de la modification.", 'error');
        }
    })
    .fail(function() {
        notification.show("Erreur de connexion au serveur.", 'error');
    });
}

 // Charger les classifications
 function loadClassifications(themeId, themeName) {
    const classificationList = $("#classification-list");
    classificationList.empty().append('<div class="loading">Chargement...</div>');

    selectedThemeId = themeId;
    selectedThemeName = themeName;

    $("#current-theme").text(selectedThemeName);

    $.getJSON("R_ThemesClass.php", { id: themeId })
        .done(function (data) {
            classificationList.empty();
            if (data.success) {
                data.data.forEach(classification => {
                    const classificationCard = $(`
                        <div class="classification-card">
                            <h3>${classification.Like_expression}</h3>
                            
                            <div class="card-actions">
                                <button class="btn btn-secondary edit-btn">
                                    <i class="fas fa-edit"></i> Modifier
                                </button>
                                <button class="btn btn-danger delete-btn">
                                    <i class="fas fa-trash"></i> Supprimer
                                </button>
                            </div>

                            <div class="card-actions">
                                <button onClick="updateHybrideStatus('${(classification.Hybride=='Y'?'N':'Y')}', ${classification.IDMapping}, '${classification.Like_expression.replace(/'/g,"\\'")}')" class="btn btn-${(classification.Hybride=='Y'?'primary':'danger')} hybride-btn mx-auto">
                                    <i class="fas fa-wand-magic-sparkles"></i> ${(classification.Hybride=='Y'?'Plus hybride':'Rendre hybride')}
                                </button>
                            </div>
                        </div>
                    `);

                    // Gestionnaire de modification
                    classificationCard.find('.edit-btn').click(function(e) {
                        e.stopPropagation();
                        const newExpression = prompt(
                            "Modifier l'expression :",
                            classification.Like_expression.replace(/%/g,"")
                        );
                        
                        if (newExpression && newExpression.length <= 255) {
                            $.getJSON("U_ThemesClass.php", {
                                theme: themeId,
                                exp: '%'+newExpression+'%',
                                IDMapping: classification.IDMapping,
                                Hybride: classification.Hybride
                            })
                            .done(function (response) {
                                if (response.success) {
                                    loadClassifications(themeId, selectedThemeName);
                                    notification.show("Classification modifiée avec succès.");
                                } else {
                                    notification.show(response.error || "Erreur lors de la modification.", 'error');
                                }
                            })
                            .fail(function() {
                                notification.show("Erreur de connexion au serveur.", 'error');
                            });
                        }
                    });

                    // Gestionnaire de suppression
                    classificationCard.find('.delete-btn').click(function(e) {
                        e.stopPropagation();
                        if (confirm("Voulez-vous vraiment supprimer cette classification ?")) {
                            $.getJSON("D_ThemeClass.php", {
                                idMapping: classification.IDMapping,
                                idTheme: 0
                            })
                            .done(function (response) {
                                if (response.success) {
                                    loadClassifications(themeId, selectedThemeName);
                                    notification.show("Classification supprimée avec succès.");
                                } else {
                                    notification.show(response.error || "Erreur lors de la suppression.", 'error');
                                }
                            })
                            .fail(function() {
                                notification.show("Erreur de connexion au serveur.", 'error');
                            });
                        }
                    });

                    classificationList.append(classificationCard);
                });
                
                $("#themes").fadeOut(300, function() {
                    $("#classifications").fadeIn(300);
                });
            } else {
                notification.show(data.error || "Erreur lors du chargement des classifications.", 'error');
            }
        })
        .fail(function() {
            notification.show("Erreur de connexion au serveur.", 'error');
        });
}

$(document).ready(function () {

    // Charger les thèmes
    function loadThemes() {

        const themeList = $("#theme-list");
        themeList.empty().append('<div class="loading">Chargement...</div>');

        $.getJSON("R_Themes.php")
            .done(function (data) {
                themeList.empty();
                if (data.success) {
                    data.data.forEach(theme => {
                        const themeCard = $(`
                            <div class="theme-card">
                                <h3>${theme.LibelleTheme}</h3>
                                <button id="voirClass${theme.IDTheme}" onClick="loadClassifications(${theme.IDTheme}, '${theme.LibelleTheme.replace(/'/,' ')}')" class="btn btn-primary">
                                    <i class="fas fa-arrow-right"></i> Voir les classifications
                                </button><br/>
                                <button class="btn btn-danger" onClick="nettoyerTheme(${theme.IDTheme})">
                                    <i class="fas fa-arrow-right"></i> Nettoyer les classifications
                                </button>
                            </div>
                        `).data("id", theme.IDTheme);

                        themeList.append(themeCard);
                    });
                } else {
                    notification.show(data.error || "Erreur lors du chargement des thèmes.", 'error');
                }
            })
            .fail(function() {
                notification.show("Erreur de connexion au serveur.", 'error');
            });
    }

    // Ajouter une nouvelle classification
    $("#add-classification-form").submit(function (e) {
        e.preventDefault();
        const newExpression = $("#new-expression").val().trim();
        
        if (newExpression && newExpression.length <= 255) {
            $.getJSON("C_ThemesClass.php", {
                id: selectedThemeId,
                expression: '%'+newExpression+'%'
            })
            .done(function (response) {
                if (response.success) {
                    $("#new-expression").val("");
                    loadClassifications(selectedThemeId, selectedThemeName);
                    notification.show("Classification ajoutée avec succès.");
                } else {
                    notification.show(response.error || "Erreur lors de l'ajout.", 'error');
                }
            })
            .fail(function() {
                notification.show("Erreur de connexion au serveur.", 'error');
            });
        } else {
            notification.show("L'expression doit contenir entre 1 et 255 caractères.", 'error');
        }
    });

    // Gestionnaire de retour à la liste des thèmes
    $("#back-to-themes").click(function () {
        $("#classifications").fadeOut(300, function() {
            $("#themes").fadeIn(300);
            selectedThemeId = null;
            $("#current-theme").text("");
            loadThemes();
        });
    });

    // Gestion des erreurs globales Ajax
    $(document).ajaxError(function(event, jqXHR, settings, error) {
        notification.show("Erreur de connexion au serveur : " + error, 'error');
    });

    // Animation de chargement pendant les requêtes Ajax
    $(document).ajaxStart(function() {
        $('body').addClass('loading');
    }).ajaxStop(function() {
        $('body').removeClass('loading');
    });

    // Initialisation : charger les thèmes au démarrage
    loadThemes();
});