(function($) {
  $.fn.fallingFlowers = function(options) {
    var settings = $.extend({
      speed: 3000, // Vitesse de chute
      density: 1000 // Densité de fleurs
    }, options);

    var screenWidth = $(window).width();

    return this.each(function() {
      var $container = $(this);

      setInterval(function() {
        var startX = Math.random() * screenWidth;
        var endX = Math.random() * screenWidth;
        var $flower = $("<div class='flower'></div>").css({
          left: startX,
          top: -30
        });

        $container.append($flower);

        $flower.animate({
          top: $(window).height()
        }, settings.speed, function() {
          $(this).remove();
        });
      }, settings.density);
    });
  };
})(jQuery);