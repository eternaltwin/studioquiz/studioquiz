<?php 

//session cross to sub domain
//ini_set('session.cookie_domain', substr($_SERVER['SERVER_NAME'],strpos($_SERVER['SERVER_NAME'],"."),100));
session_start(); 

if (!empty($_COOKIE["userid_dq"]))
{
	$_SESSION["userid_dq"] = $_COOKIE["userid_dq"];
}

if (!empty($_COOKIE["pseudo_dq"]))
{
	$_SESSION["pseudo_dq"] = $_COOKIE["pseudo_dq"];
}

if (!isset($_SESSION['userid_dq']))
{
	header('Location: index.php'); 
}

?>
<!DOCTYPE html>
<html lang="fr"><!-- Basic -->
<head>
	<base href="/">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
 
     <!-- Site Metas -->
    <title>Directquiz - Gestion du profil</title> 
	<link rel="manifest" href="/manifest.json">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon" />	
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

		<!-- ALL JS FILES -->
	<script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	
	<script src="js/lobibox.js" type="text/javascript"></script>
	<script src="js/core.js" type="text/javascript"></script>
	
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css">
	<!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
	<!-- Alert CSS -->
    <link rel="stylesheet" href="css/lobibox.css">   
	
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	
	<script>
	
	function generateUUID() {
			return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				const r = Math.random() * 16 | 0;
				const v = c === 'x' ? r : (r & 0x3 | 0x8);
				return v.toString(16);
			});
	}
	
	$(function(){
		
					$.post("php/getLevelByUUID.php",{uuid:'<?= $_SESSION['userid_dq'] ?>'}).done(function(data){

						var result = data.split("#");
						//Level # UUID
	
						if (result[1]!="")
						{
							$(".ceinture").html("<img class='ceinture-profil' src='images/ceinture_"+result[0].trim()+".png' />");
							$(".ceinture2").html("<img class='ceinture-profil-menu' src='images/ceinture_"+result[0].trim()+".png' />");
							$("#XP").text(result[2].trim());
							
																				
							$("#profil-thune-cadre").html("Fortune : " + result[3].trim()+" <img class='piecetteG' title='DirectDollar' src='images/dd.png' />");
						}
						
						if (result[3]!="")
						{
							$("#directdollar-menu").text(result[3].trim());
						}
					}).
					fail(function(){
						
						
						
					});
					
					
					
			var $uploadContainer = $('#uploadContainer');
            var $fileInput = $('#fileInput');
            var $result = $('#result');

            // Gestion du clic sur le bouton pour ouvrir le sélecteur de fichiers
            $('#selectFileButton').on('click', function() {
                $fileInput.click();
            });

            // Gestion de la sélection de fichier
            $fileInput.on('change', function(e) {
                uploadFile(this.files[0]);
            });

            // Gestion du drag and drop
            $uploadContainer.on('dragover', function(e) {
                e.preventDefault();
                e.stopPropagation();
                $uploadContainer.addClass('dragging');
            });

            $uploadContainer.on('dragleave', function(e) {
                e.preventDefault();
                e.stopPropagation();
                $uploadContainer.removeClass('dragging');
            });

            $uploadContainer.on('drop', function(e) {
                e.preventDefault();
                e.stopPropagation();
                $uploadContainer.removeClass('dragging');

                var files = e.originalEvent.dataTransfer.files;
                if (files.length > 0) {
                    uploadFile(files[0]);
                }
            });

            // Fonction d'upload de fichier
            function uploadFile(file) {
                var formData = new FormData();
                formData.append('avatar', file);
                formData.append('idMember', $("#idMember").val());

                $.ajax({
                    url: 'https://www.directquiz.org:3000/upload',
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response == 'OK')
						{
							
							Lobibox.alert(
							
								'success', { msg: "Votre avatar a été mis à jour avec succès !"}
							
							); 
							
						}
						else
						{
							if (response == "KO_SIZE")
							{
								Lobibox.alert(							
									'error', { msg: "Votre image pèse trop lourd (5 Mo maximum !!) !"}							
								); 
							}
							else if (response == "KO_TYPE")
							{
								Lobibox.alert(							
									'error', { msg: "Votre image n'est pas au format JPEG !"}							
								); 
							}
							else
							{
								Lobibox.alert(
								
									'error', { msg: "Une erreur d'ordre inconnu est survenue !"}
								
								); 
							}
						}
						
       
						
						$("#currentAvatarGestion").attr("src","images/avatar/<?= $_SESSION['userid_dq'] ?>.jpg?"+generateUUID())
                    },
                    error: function(xhr, status, error) {
						
                        	Lobibox.alert(
								
								'error', { msg: "Une erreur fatale est survenue !"}
							
							); 
                    }
                });
            }
		
	});
		
	</script>
	
	<style>
	
		table
		{
			
			 width: 90%!important;
			
		}
	
	 .upload-container {
            border: 2px dashed #fff;
            padding: 30px;
            text-align: center;
            background-color: #793ac7;
            width: 85%;
            transition: border-color 0.3s ease;
        }
        .upload-container.dragging {
            border-color: #666;
        }
        .upload-container p {
            margin: 0;
            font-size: 16px;
            color: #666;
        }
        .upload-container input[type="file"] {
            display: none;
        }
        .upload-container button {
            margin-top: 10px;
            padding: 10px 20px;
            background-color: #4CAF50;
            color: #fff;
            border: none;
            cursor: pointer;
            border-radius: 4px;
            transition: background-color 0.3s ease;
        }
        .upload-container button:hover {
            background-color: #45a049;
        }
        #result {
            margin-top: 20px;
            font-size: 16px;
            color: #333;
        }
		
		.instruction
		{
			color : #fff!important;
		}
	
	</style>
	
</head>
<body id="contact_page" data-spy="scroll" data-target="#navbar-wd" data-offset="98" class="fs-grid demo_page">

	<!-- LOADER -->
    <div id="preloader">
		<div class="loader">
			<img src="images/loader.gif" alt="#"/>
		</div>
    </div><!-- end loader -->
    <!-- END LOADER -->
	
	<!-- Start header -->
	<header class="top-header">
		<nav class="navbar header-nav navbar-expand-lg">
            <div class="container-fluid">
				<a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="image"></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
					<span></span>
					<span></span>
					<span></span>
				</button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav">
                        <li><a class="nav-link" href="index.php">Accueil</a></li>
        
						<li><a class="nav-link" href="jouer.php">Rejoindre une partie</a></li>
						<?php if (!isset($_SESSION['userid_dq'])) { ?>
                        <li><a class="nav-link" href="login.php">Inscription / Connexion</a></li> <!-- Script qui reviendra à jouer -->
						<?php } ?>
						<li><a class="nav-link" href="classement.php">Classement</a></li>
						<?php if (isset($_SESSION['userid_dq'])) { ?>
						<li><a class="nav-link" href="validationQuestion.php">Proposer</a></li>
						<li><a class="nav-link active" href="profil.php">Profil (<?= $_SESSION['pseudo_dq'] ?> <span id="ceinture-menu" class="ceinture2"></span> | <span id="directdollar-menu"></span> <img class='piecette' title='DirectDollar' src='images/dd.png'>)</a></li>
					  <li><a class="nav-link" href="discordEndPoint.php"><image src="images/discord_chat.png"></image> Lier</a></li>
					  <li><a class="nav-link" href="logoff.php">Se déconnecter</a></li> <!-- Script qui reviendra accueil -->
						<?php } ?>
				
                    </ul>
                </div>
				<div class="search-box">

	             </div>
            </div>
        </nav>
	</header>
	<!-- End header -->

	    <div class="section layout_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="full center">
                        <div class="heading_main text_align_center">
                            <h2><span class="theme_color">GESTION</span> DU PROFIL</h2>
                            <p class="large"><?= $_SESSION['pseudo_dq'] ?></p>
											
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	
	    <!-- section -->
    <div class="section layout_padding theme_bg">
        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12 white_fonts">

					<table id="table-profil">
					  	
					
						<tr><td colspan="2">
						
							<div class="card" style="text-align:center; color:white; font-weigth: bold; width: 100%; padding: 15px; background-color: #ab13ff;">

								  <div style="display:inline-block;">
								  
											<p style="font-size: 2.3em;"><span id="pseudo-profil"><?= $_SESSION['pseudo_dq'] ?></span> <span id="ceinture" class="ceinture"></span>  | XP : <span id="XP"></span> </p>
								  
								  </div>
								  
								  <div class="card-body">
								  
											<div id="profil-thune-cadre"></div>
											
								  </div>
								  
							</div>
								
						</td></tr>	
						
<tr><td>Avatar actuel : </td><td style="padding-left:30px;"><br/><br/><img id="currentAvatarGestion" src="images/avatar/<?= $_SESSION['userid_dq'] ?>.jpg?<?= time(); ?>"></img></td></tr>
						
						<tr><td colspan="2"><hr style="border:2px solid white;" /></td></tr>
						<tr><td>Choisir un avatar : 
						</td><td colspan="2">

							<!-- Système d'upload ici -->
				
						<div class="upload-container" id="uploadContainer">
							<p class="instruction">Glissez et déposez un avatar ici</p>
							<p>ou</p>
							<button id="selectFileButton">Sélectionner un fichier</button>
							<input type="file" name="avatar" id="fileInput">
							<input id="idMember" value="<?= $_SESSION['userid_dq'] ?>" type="hidden" />
						</div>
						<div id="result"></div>

						<br/>
						</td></tr>

					</table>
					
			<br/><br/>

        <p><strong>Note :</strong> Format accepté : .jpg | Taille maximum : 5 Mo</p>
		
						<br/>

				
                </div>
            </div>
        </div>
    </div>


	<?php 
	
		include('footer.php');
	
	?>



	<a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>


	<script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script> 
	<script src="js/slider-index.js"></script>
	<script src="js/smoothscroll.js"></script>
	<script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
	<script src="js/isotope.min.js"></script>	
	<script src="js/images-loded.min.js"></script>	
    <script src="js/custom.js"></script>
</body>
</html>

